import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:meroDiary/screens/add_task.dart';
import 'package:meroDiary/task_bloc/task_bloc.dart';
import 'package:meroDiary/utils/dependency_injection.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  void initState() {
    inject<TaskBloc>().add(GetTaskEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            icon: Icon(Icons.add_box_outlined),
            onPressed: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => AddTask()));
            },
          ),
        ],
      ),
      body: BlocConsumer<TaskBloc, TaskState>(
        cubit: inject<TaskBloc>(),
        listener: (context, state) {},
        builder: (context, state) {
          if (state is TaskLoadingState) {
            return Center(
              child: SpinKitFadingFour(
                color: Colors.green,
              ),
            );
          } else if (state is TaskLoadedState) {
            if (state.taskModel.length > 0) {
              return ListView.separated(
                padding: EdgeInsets.all(10),
                separatorBuilder: (context, index) {
                  return Divider();
                },
                itemCount: state.taskModel.length,
                itemBuilder: (context, index) {
                  return Text("${state.taskModel[index].taskId}");
                },
              );
            } else {
              return Center(
                child: Text("No Task Added yet"),
              );
            }
          } else {
            return Center(
              child: SpinKitFadingFour(
                color: Colors.green,
              ),
            );
          }
        },
      ),
    );
  }
}
