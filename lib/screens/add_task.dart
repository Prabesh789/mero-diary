import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meroDiary/models/task_model.dart';
import 'package:meroDiary/task_bloc/task_bloc.dart';
import 'package:meroDiary/utils/dependency_injection.dart';
import 'package:uuid/uuid.dart' as uuid;

class AddTask extends StatefulWidget {
  @override
  _AddTaskState createState() => _AddTaskState();
}

class _AddTaskState extends State<AddTask> {
  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formkey = GlobalKey<FormState>();

  String taskId;

  @override
  void initState() {
    taskId = uuid.Uuid().v1();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<TaskBloc, TaskState>(
      cubit: inject<TaskBloc>(),
      listener: (context, state) {
        if (state is TaskErrorState) {
          _scaffoldKey.currentState.showSnackBar(
            SnackBar(
              backgroundColor: Colors.red[700],
              duration: Duration(seconds: 2),
              content: Text("${state.errorMessage}"),
            ),
          );
        }

        if (state is TaskAddedState) {
          _scaffoldKey.currentState.showSnackBar(
            SnackBar(
              backgroundColor: Colors.green[700],
              duration: Duration(seconds: 2),
              content: Text("${state.message}"),
            ),
          );
          titleController.clear();
          descriptionController.clear();
          dateController.clear();
          taskId = uuid.Uuid().v1();
        }
      },
      builder: (context, state) {
        return Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(),
          body: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Form(
                key: _formkey,
                child: Column(
                  children: [
                    TextFormField(
                      controller: titleController,
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Please enter title";
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                          floatingLabelBehavior: FloatingLabelBehavior.auto,
                          labelText: "Enter task title",
                          hintText: "Enter task title"),
                    ),
                    TextFormField(
                      controller: descriptionController,
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Please enter description";
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                          floatingLabelBehavior: FloatingLabelBehavior.auto,
                          labelText: "Enter task description",
                          hintText: "Enter task description"),
                    ),
                    TextFormField(
                      controller: dateController,
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Please enter date";
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        floatingLabelBehavior: FloatingLabelBehavior.auto,
                        labelText: "Enter task deadline",
                        hintText: "Enter task deadline",
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              if (!_formkey.currentState.validate()) {
                _scaffoldKey.currentState.showSnackBar(
                  SnackBar(
                    backgroundColor: Colors.blueGrey,
                    duration: Duration(seconds: 1),
                    content: Text("Please fill all fields !"),
                  ),
                );
              } else {
                inject<TaskBloc>().add(
                  AddTaskEvent(
                    taskModel: TaskModel(
                      taskId: taskId,
                      taskDeadline: dateController.text.trim(),
                      taskDescription: descriptionController.text.trim(),
                      tasktitle: titleController.text.trim(),
                    ),
                  ),
                );
              }
            },
            child: Icon(Icons.add),
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    inject<TaskBloc>().add(GetTaskEvent());
    super.dispose();
  }
}
