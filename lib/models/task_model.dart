import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meroDiary/hive_db_setup/hive_const.dart';

part 'task_model.g.dart';

@HiveType(typeId: HIVE_TO_DO_BOX_ID)
@JsonSerializable()
// ignore: must_be_immutable
class TaskModel extends HiveObject implements Equatable {
  @HiveField(0)
  String taskId;
  @HiveField(1)
  String tasktitle;
  @HiveField(2)
  String taskDescription;
  @HiveField(3)
  String taskDeadline;

  TaskModel({
    @required this.taskId,
    @required this.tasktitle,
    @required this.taskDescription,
    @required this.taskDeadline,
  });

  factory TaskModel.fromJson(Map<String, dynamic> json) =>
      _$TaskModelFromJson(json);
  Map<String, dynamic> toJson() => _$TaskModelToJson(this);

  @override
  List<Object> get props => throw UnimplementedError();

  @override
  bool get stringify => throw UnimplementedError();
}
