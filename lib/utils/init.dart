import 'package:flutter/material.dart';
import 'package:meroDiary/hive_db_setup/hive_setup.dart';
import 'package:meroDiary/utils/dependency_injection.dart';

class AppInit {
//

  static Future<void> initialize() async {
    //

    await _externalSetup();
  }

  /// Startup initialization
  static Future<void> _externalSetup() async {
    WidgetsFlutterBinding.ensureInitialized();

    // Initialization of Dependency Injection
    await initDependencyInjection();

    // Hive setup
    await HiveSetup.initHive;
  }
}
