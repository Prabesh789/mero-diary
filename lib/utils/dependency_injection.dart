import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:meroDiary/hive_db_setup/local_data_source.dart';
import 'package:meroDiary/hive_db_setup/local_data_source_impl.dart';
import 'package:meroDiary/repository/task_repository.dart';
import 'package:meroDiary/task_bloc/task_bloc.dart';

final GetIt inject = GetIt.instance;

Future<void> initDependencyInjection() async {
  WidgetsFlutterBinding.ensureInitialized();

  _registerBlocs();
  _registerRepositories();

  _registerLocalDatabase();
}

void _registerLocalDatabase() {
  inject.registerLazySingleton<LocalDataSource>(() => LocalDataSourceImpl());
}

void _registerBlocs() {
  inject.registerLazySingleton(
    () => TaskBloc(
      taskRepository: inject(),
    ),
  );
}

void _registerRepositories() {
  inject.registerLazySingleton(
    () => TaskRepository(
      localDataSource: inject(),
    ),
  );
}
