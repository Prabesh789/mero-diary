import 'package:flutter/material.dart';
import 'package:meroDiary/hive_db_setup/hive_setup.dart';
import 'package:meroDiary/screens/home.dart';
import 'package:meroDiary/utils/init.dart';

void main() async {
  await AppInit.initialize();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Home(),
    );
  }
}
