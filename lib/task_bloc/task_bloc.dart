import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:meroDiary/models/task_model.dart';
import 'package:meroDiary/repository/task_repository.dart';

part 'task_event.dart';
part 'task_state.dart';

class TaskBloc extends Bloc<TaskEvent, TaskState> {
  final TaskRepository _taskRepository;
  TaskBloc({
    @required TaskRepository taskRepository,
  })  : assert(taskRepository != null),
        _taskRepository = taskRepository,
        super(TaskLoadingState());
  @override
  Stream<TaskState> mapEventToState(
    TaskEvent event,
  ) async* {
    if (event is AddTaskEvent) {
      yield TaskLoadingState();

      final response = await _taskRepository.addTask(event.taskModel);

      if (response) {
        yield TaskAddedState(message: "Task Added !");
      } else {
        yield TaskErrorState(errorMessage: "Couldn't add your task ! ");
      }
    }

    if (event is GetTaskEvent) {
      yield TaskLoadingState();
      final response = await _taskRepository.getTask();
      yield TaskLoadedState(
        taskModel: response,
      );
    }

    if (event is UpdateTaskEvent) {}

    if (event is DeleteTaskEvent) {}
  }
}
