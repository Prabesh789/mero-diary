part of 'task_bloc.dart';

abstract class TaskEvent extends Equatable {}

class AddTaskEvent extends TaskEvent {
  final TaskModel taskModel;
  AddTaskEvent({
    @required this.taskModel,
  });
  @override
  List<Object> get props => [taskModel];
}

class UpdateTaskEvent extends TaskEvent {
  final TaskModel taskModel;
  UpdateTaskEvent({
    @required this.taskModel,
  });
  @override
  List<Object> get props => [taskModel];
}

class GetTaskEvent extends TaskEvent {
  @override
  List<Object> get props => [];
}

class DeleteTaskEvent extends TaskEvent {
  @override
  List<Object> get props => [];
}
