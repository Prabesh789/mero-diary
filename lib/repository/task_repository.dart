import 'package:flutter/material.dart';
import 'package:meroDiary/hive_db_setup/local_data_source.dart';
import 'package:meroDiary/models/task_model.dart';

class TaskRepository {
  final LocalDataSource _localDataSource;

  TaskRepository({
    @required LocalDataSource localDataSource,
  })  : assert(localDataSource != null),
        _localDataSource = localDataSource;

  Future<bool> addTask(TaskModel taskModel) async {
    try {
      final response = await _localDataSource.addTask(taskModel: taskModel);
      return response;
    } catch (e) {
      return false;
    }
  }

  Future<List<TaskModel>> getTask() async {
    try {
      final response = await _localDataSource.getTask();
      return response;
    } catch (e) {
      return [];
    }
  }
}
