import 'package:meroDiary/models/task_model.dart';

abstract class LocalDataSource {
  Future<bool> addTask({TaskModel taskModel});
  Future<List<TaskModel>> getTask();
}
