import 'package:meroDiary/hive_db_setup/hive_const.dart';
import 'package:meroDiary/hive_db_setup/hive_setup.dart';
import 'package:meroDiary/hive_db_setup/local_data_source.dart';
import 'package:meroDiary/models/task_model.dart';

class LocalDataSourceImpl implements LocalDataSource {
  @override
  Future<bool> addTask({TaskModel taskModel}) async {
    try {
      final hiveTodoBox = await openBox(HIVE_TO_DO_BOX);
      final hiveTodoListBox = await openBox(HIVE_TO_DO_LIST_BOX);

      List<String> todoIdList = await hiveTodoListBox.get(0) ?? [];
      if (!todoIdList.contains(taskModel.taskId)) {
        todoIdList.add(taskModel.taskId);
      }

      await hiveTodoBox.put(taskModel.taskId, taskModel);

      await hiveTodoListBox.put(0, todoIdList);

      return true;
    } catch (e) {
      return false;
    }
  }

  @override
  Future<List<TaskModel>> getTask() async {
    final hiveTodoBox = await openBox(HIVE_TO_DO_BOX);
    final hiveTodoIdList = await openBox(HIVE_TO_DO_LIST_BOX);
    if (hiveTodoBox.length != 0) {
      List<String> draftOfferList = await hiveTodoIdList.get(0) ?? [];

      List<TaskModel> offerList = <TaskModel>[];
      for (int i = 0; i < draftOfferList.length; i++) {
        offerList.add(await hiveTodoBox.getAt(i));
      }

      return offerList;
    } else {
      return [];
    }
  }
}
