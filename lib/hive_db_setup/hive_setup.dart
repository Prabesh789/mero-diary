import 'package:meroDiary/models/task_model.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:hive/hive.dart';

class HiveSetup {
  //initialize class
  HiveSetup._();
//setter method to initialize hive directory
  static Future get initHive => _initHive();

  static Future _initHive() async {
    final appDocumentDir =
        await path_provider.getApplicationDocumentsDirectory();
    Hive..init(appDocumentDir.path);
    Hive.registerAdapter(TaskModelAdapter());
  }
}

Future<LazyBox> openBox(String name) async {
  return Hive.isBoxOpen(name)
      ? Hive.lazyBox(name)
      : await Hive.openLazyBox(name);
}
